package common

import (
	"log"
	"time"
)

// PrintStatsPerSec prints statistics about bytes sent/received per second
func PrintStatsPerSec(countCh <-chan uint64) {
	var totalPerSecond uint64 = 0
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case count := <-countCh:
			totalPerSecond += count
		case <-ticker.C:
			megaByte := float64(totalPerSecond) / (1024 * 1024)
			gigaBit := float64(totalPerSecond*8) / 1_000_000_000
			log.Printf("%f MB / %f GigaBit", megaByte, gigaBit)
			totalPerSecond = 0
		}
	}
}
