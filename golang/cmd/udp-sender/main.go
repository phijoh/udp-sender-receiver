package main

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"os/signal"
	"syscall"
	"udp-lab/common"
)

func createUdpConnection(hostPort string) (*net.UDPConn, error) {
	recAddr, err := net.ResolveUDPAddr("udp", hostPort)
	if err != nil {
		return nil, err
	}
	return net.DialUDP("udp", nil, recAddr)
}

func main() {
	target := flag.String("target", "localhost:1100", "host:port of target")
	size := flag.Int("size", 8900, "size of payload (in Byte)")
	flag.Parse()

	conn, err := createUdpConnection(*target)
	if err != nil {
		log.Panic("error creating udp connection", err)
	}

	totalBytesSent := big.NewInt(0)

	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		<-sigs
		fmt.Println("TOTAL BYTES SENT:", totalBytesSent)
		os.Exit(0)
	}()

	countCh := make(chan uint64, 100)
	go common.PrintStatsPerSec(countCh)

	// just writing zeros
	buf := make([]byte, *size)
	for {
		n, err := conn.Write(buf)
		if err != nil {
			log.Panic("error writing to udp connection", err)
		}
		if n != *size {
			log.Panic("wrote only", n, "of", *size, "bytes")
		}
		countCh <- uint64(n)
		totalBytesSent.Add(totalBytesSent, big.NewInt(int64(n)))
	}
}
