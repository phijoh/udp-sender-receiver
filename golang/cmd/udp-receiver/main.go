package main

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"os/signal"
	"syscall"
	"udp-lab/common"
)

func createUdpConnection(hostPort string) (*net.UDPConn, error) {
	addr, err := net.ResolveUDPAddr("udp", hostPort)
	if err != nil {
		return nil, err
	}
	return net.ListenUDP("udp", addr)
}

func main() {
	hostPort := flag.String("hostPort", "localhost:1100", "host:port to listen on")
	flag.Parse()

	conn, err := createUdpConnection(*hostPort)
	if err != nil {
		log.Panic("error creating udp connection", err)
	}

	countCh := make(chan uint64, 100)
	go common.PrintStatsPerSec(countCh)
	totalBytesReceived := big.NewInt(0)

	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		<-sigs
		fmt.Println("TOTAL BYTES RECEIVED:", totalBytesReceived)
		os.Exit(0)
	}()

	buf := make([]byte, 9000)
	oobBuf := make([]byte, 9000)
	for {
		n, oobn, flags, _, err := conn.ReadMsgUDP(buf, oobBuf)
		if err != nil {
			log.Panic("error reading from udp connection", err)
		}
		if oobn != 0 {
			msg := fmt.Sprintf("got oob data %v and flags %v", oobBuf[:oobn], flags)
			log.Panic(msg, err)
		}
		countCh <- uint64(n)
		totalBytesReceived.Add(totalBytesReceived, big.NewInt(int64(n)))
	}

}
