all: build

build:
	@cd golang && go build --trimpath -o ../go-udp-sender cmd/udp-sender/main.go
	@cd golang && go build --trimpath -o ../go-udp-receiver cmd/udp-receiver/main.go
	@gcc c/sender.c -o c-udp-sender
	@gcc c/receiver.c -o c-udp-receiver

clean:
	@rm -f go-udp-sender go-udp-receiver c-udp-sender c-udp-receiver