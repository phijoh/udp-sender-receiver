# UDP Throughput Test

Minimal UDP sender and receiver to assess throughput. Transmits UDP packets of specified size with dummy data as fast as possible and prints statistics.
One implementation in golang and one in C.