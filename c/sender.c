// Client side implementation of UDP client-server model
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

#define SIZE 8900

int main(int argc, char **argv)
{
	if (argc < 3)
	{
		printf("Need: host port\n");
		return 1;
	}

	char *host = argv[1];
	int port = atoi(argv[2]);
	int sockfd;
	char buffer[SIZE];
	struct sockaddr_in servaddr;

	// Creating socket file descriptor
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}
	{
		int broadcastEnable = 1;
		int ret = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
		if (ret < 0)
		{
			perror("setting socket options failed");
			exit(EXIT_FAILURE);
		}
	}

	memset(&servaddr, 0, sizeof(servaddr));

	// Filling server information
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr(host);
	servaddr.sin_port = htons(port);

	// bcast_sock = socket(AF_INET, SOCK_DGRAM, 0);
	// int broadcastEnable=1;
	// int ret=setsockopt(bcast_sock, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));

	int n = 0;
	double counter = 0;
	time_t last;
	time_t now;

	time(&last);
	while (1)
	{
		n = sendto(sockfd, (const char *)buffer, SIZE,
							 MSG_CONFIRM, (const struct sockaddr *)&servaddr,
							 sizeof(servaddr));
		if (n < 0)
		{
			perror("error sending udp");
			continue;
		}
		counter += n;
		time(&now);
		if (last < now)
		{
			double megaBytes = counter / 1000000;
			double gigaBit = counter * 8 / 1000000000;
			printf("sent %f MB (%f G)\n", megaBytes, gigaBit);
			counter = 0;
			last = now;
		}
	}

	close(sockfd);
	return 0;
}