// Server side implementation of UDP client-server model
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define SIZE 9000

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    printf("Need: host port\n");
    return 1;
  }

  char *host = argv[1];
  int port = atoi(argv[2]);

  int sockfd;
  char buffer[SIZE];
  struct sockaddr_in servaddr, cliaddr;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));
  memset(&cliaddr, 0, sizeof(cliaddr));

  // Filling server information
  servaddr.sin_family = AF_INET; // IPv4
  servaddr.sin_addr.s_addr = inet_addr(host);
  servaddr.sin_port = htons(port);

  // Bind the socket with the server address
  if (bind(sockfd, (const struct sockaddr *)&servaddr,
           sizeof(servaddr)) < 0)
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  int len = sizeof(cliaddr);
  int n = 0;
  double counter = 0;
  time_t last;
  time_t now;

  time(&last);
  while (1)
  {
    n = recvfrom(sockfd, (char *)buffer, SIZE,
                 MSG_WAITALL, (struct sockaddr *)&cliaddr,
                 &len);
    counter += n;
    time(&now);
    if (last < now)
    {
      double megaBytes = counter / 1000000;
      double gigaBit = counter * 8 / 1000000000;
      printf("got %f MB (%f G)\n", megaBytes, gigaBit);
      counter = 0;
      last = now;
    }
  }

  return 0;
}